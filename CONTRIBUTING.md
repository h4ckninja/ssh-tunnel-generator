# Contributing

I welcome merge requests, feature requests, bug reports, ideas, and suggestions!

This project is maintained in my spare time. I have a full-time job and other time constraints that keep me away. If I don't respond, I'm just busy.

I'm also slowly learning Ruby, so it may take me a bit more time to figure something out. Please feel free to give me pointers or send a merge request. :)

## Code Style

You've probably noticed by now that my code style is very different from the "recommended" Ruby style. I use tabs instead of spaces, and I use an indent of `4` instead of `2`. I also take the long way to do certain things instead of shortcuts.

I do this because I want readability over optimization. I realize that what some may consider readable is not necessary what someone else may consider readable. But it works for me.

This all to say that if you send a merge request, please use my code style.
