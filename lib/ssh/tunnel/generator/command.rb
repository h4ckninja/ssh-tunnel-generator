module SSH::Tunnel::Generator
	class Command
		trap('SIGINT') { exit }


		# Execute this command
		#
		# @api public
		def execute(*)
			raise(
				NotImplementedError,
				"#{self.class}##{__method__} must be implemented"
			)
		end
	end
end

