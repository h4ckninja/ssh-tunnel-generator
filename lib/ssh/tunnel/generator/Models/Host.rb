module SSH::Tunnel::Generator::Models
	class Host
		attr_reader :short_name, :hostname, :port, :user, :identity, :kex, :ciphers, :proxy_host, :extra_options


		def initialize(short_name, hostname, port, user, identity, kex, ciphers, proxy_host, extra_options)
			@short_name = short_name
			@hostname = hostname
			@port = port
			@user = user
			@identity = identity
			@kex = kex
			@ciphers = ciphers
			@proxy_host = proxy_host
			@extra_options = extra_options
		end

		def to_s
		 	return "#{@hostname} (#{@short_name})"
		end

		def downcase
			return @hostname.downcase!
		end
	end
end
