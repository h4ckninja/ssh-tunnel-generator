require 'pastel'

require 'tty-prompt'
require 'tty-file'

require 'ostruct'

require_relative '../command'
require_relative '../Models/Host'


module SSH::Tunnel::Generator::Commands
	class Generate < SSH::Tunnel::Generator::Command
		def initialize(options)
			@pastel = Pastel.new
			@options = options
		end

		def execute(output: $stdout)
			prompt = TTY::Prompt.new()
			hosts = Array.new()
			hostsStructHolder = OpenStruct.new()
			hostsStruct = OpenStruct.new()
			continue = true
			save_file = @options['file']

			while continue
				puts "\n"

				short_name = prompt.ask('Enter short name:')
				hostname = prompt.ask('Enter the full hostname or IP:')
				port = prompt.ask('Port:', default: 22)
				user = prompt.ask('User:')
				identity = prompt.ask('Location to identity file:', default: '')
				kex = prompt.ask('Special KeyExchangeAlgorithms? (Not normal):')
				ciphers = prompt.ask('Special cipher (Not normal):')

				if hosts.empty?
					proxy_host = nil

				else
					proxy_host = prompt.select('Proxy host:', hosts, filter: true, cycle: true)
				end

				continue = prompt.yes?('Add more boxes?')


				host = SSH::Tunnel::Generator::Models::Host.new(short_name, hostname, port, user, identity, kex, ciphers, proxy_host, '')

				hosts.push(host)
			end


			hosts.each do |host|
				short_name = host.short_name

				hostsStructHolder[short_name] = host
			end

			# Surely there's a better way ...
			hostsStruct['hosts'] = hostsStructHolder


			TTY::File.copy_file("#{__dir__}/../ssh_config.erb", "#{save_file}", context: hostsStruct)
		end


		private

	end # Generate
end
