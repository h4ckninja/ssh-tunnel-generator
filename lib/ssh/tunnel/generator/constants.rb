require_relative 'version'


module SSH::Tunnel::Generator::Constants
	BUG_TRACKER_URL = 'https://gitlab.com/h4ckninja/ssh-tunnel-generator/-/issues'
	CHANGELOG_URL = "https://gitlab.com/h4ckninja/ssh-tunnel-generator/v#{SSH::Tunnel::Generator::Constants::VERSION}/changelog.html"
	SOURCE_URL = 'https://gitlab.com/h4ckninja/ssh-tunnel-generator'
	DOCUMENTATION_URL = 'https://gitlab.com/h4ckninja/ssh-tunnel-generator/wiki'
	HOMEPAGE_URL = 'https://h4ck.ninja/projects/'
end
