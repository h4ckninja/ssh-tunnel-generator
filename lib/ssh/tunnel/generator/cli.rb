require 'thor'
require 'pastel'
require 'tty-font'
require 'tty-prompt'


module SSH
	module Tunnel
		module Generator
			class CLI < Thor
				def initialize(args = [], local_options = {}, config = {})
					@pastel = Pastel.new

					super
				end

				class_option 'verbose', :aliases => '-vb', :type => :boolean, :desc => 'Be verbose.'
				class_option 'debug', :aliases => '-d', :type => :boolean, :desc => 'Debug mode.'

				map %w(--version -v) => :version

				no_commands do
					def help(*args)
						logo()

						super()
					end

					def logo
						font =  TTY::Font.new(:doom)

						puts "\n\n"
						puts @pastel.decorate(font.write('SSH  Tunnel  Generator', letter_spacing: 0.4), :yellow, :bold)
						puts "\n\n"
					end
				end

				desc 'generate', 'Create the SSH config file.'
				long_desc File.open("#{__dir__}/help/generate.txt").read
				option 'help', :aliases => '-h', :type => :boolean, :desc => 'Display usage information.'
				option 'file', :aliases => '-f', :default => "./ssh_config_#{Time.now.strftime('%F_%I-%M%p')}", :desc => 'Filename and location to save to.'
				def generate
					if options[:help]
						invoke :help, [__method__]

					else
						require_relative './Commands/generate'

						Commands::Generate.new(options).execute()
					end
				end

				desc 'version', 'Prints the version of the config generator.'
				def version
					require_relative 'version'
					require_relative 'constants'

					logo

					puts "v#{SSH::Tunnel::Generator::Constants::VERSION}"
					puts "Source code available at: #{SSH::Tunnel::Generator::Constants::SOURCE_URL}"
					puts "Documentation available at: #{SSH::Tunnel::Generator::Constants::DOCUMENTATION_URL}"
					puts "\n\n"
				end

				def self.exit_on_failure?
					return true
				end
			end
		end
	end
end
