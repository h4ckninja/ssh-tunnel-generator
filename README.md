# SSH::Tunnel::Generator

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/h4ckninja)


Creating SSH tunnels during an engagement can get complicated. This gem helps create a config file for you.


## Installation

```
$ gem install ssh-tunnel-generator
```

## Usage

```
$ ssh-tunnel-generator generate [-f filename]
```

`-f`: This is optional. You can specify a filename to save the config file as. If not provided, it defaults to today's date and time to help you keep track of your config files.

Follow the prompts. This gem is intended for simplicity rather than handling all cases, but if you have a feature idea, feel free to request!

>*Catch*: For reasons on unknown, SSH does not handle creating tunnels well when passing the config file outside of `.ssh/` in your home directory. So the file must ultimately be saved as `~/.ssh/config` for the tunnels to work. If you know why this is and how to fix it so you can use a custom config file on the SSH client command line, I'd appreciate it.


## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/h4ckninja/ssh-tunnel-generator.

[Contributing guidelines](CONTRIBUTING.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
