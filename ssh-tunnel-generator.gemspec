require_relative 'lib/ssh/tunnel/generator/version'
require_relative 'lib/ssh/tunnel/generator/constants'


Gem::Specification.new do |spec|
	spec.name = 'ssh-tunnel-generator'
	spec.version = SSH::Tunnel::Generator::Constants::VERSION
	spec.authors = ['h4ckNinja']
	spec.email = ['h4ckninja.biz@gmail.com']

	spec.summary = %q{This is a simple command line tool to assist in generating SSH config files for creating tunnels.}
	spec.description = %q{Useful in pentesting or red team exercises, SSH tunnels can be annoying to create. This tool helps create a config file for you.}
	spec.homepage = SSH::Tunnel::Generator::Constants::HOMEPAGE_URL
	spec.license = 'MIT'
	spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')
	spec.post_install_message = 'Thanks for installing!'

	#spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

	spec.metadata = {
		'bug_tracker_uri' => SSH::Tunnel::Generator::Constants::BUG_TRACKER_URL,
		'changelog_uri' => SSH::Tunnel::Generator::Constants::CHANGELOG_URL,
		'documentation_uri' => SSH::Tunnel::Generator::Constants::DOCUMENTATION_URL,
		'homepage_uri' => SSH::Tunnel::Generator::Constants::HOMEPAGE_URL,
		'source_code_uri' => SSH::Tunnel::Generator::Constants::SOURCE_URL,
		'wiki_uri' => SSH::Tunnel::Generator::Constants::DOCUMENTATION_URL
	}

	# Specify which files should be added to the gem when it is released.
	# The `git ls-files -z` loads the files in the RubyGem that have been added into git.
	spec.files = Dir.chdir(File.expand_path('..', __FILE__)) do
		`git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
	end

	spec.bindir = 'exe'
	spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
	spec.require_paths = ['lib']

	# Dependencies
	spec.add_runtime_dependency 'thor'
	spec.add_runtime_dependency 'tty-color'
	spec.add_runtime_dependency 'tty-command'
	spec.add_runtime_dependency 'tty-file'
	spec.add_runtime_dependency 'tty-font'
	spec.add_runtime_dependency 'tty-prompt'
	spec.add_runtime_dependency 'pastel'
end
